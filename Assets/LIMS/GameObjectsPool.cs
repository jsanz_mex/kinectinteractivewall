﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolInstance
{
    public UnityEngine.Object instance;
    public bool inUse = false;
}

/// <summary>
/// Pool prefab's model.
/// </summary>
[System.Serializable]
public class PrefabPoolConfiguration
{
    public UnityEngine.Object prefab;
    public int capacity;
    [HideInInspector] public PoolInstance[] instances;
    [HideInInspector] public int it = 0;
}

/// <summary>
/// This class wraps methods to create and manage pools of game objects. 
/// Use it when you need to recycle game objects. Instead of create/destroy them.
/// </summary>
public class GameObjectsPool : MonoBehaviour
{

    #region INSPECTOR
    public bool createPoolOnAwake = false;
    public PrefabPoolConfiguration[] prefabs;
    #endregion

    #region CONSTANTS
    private const float X_VAL = 10000;
    private const float Y_VAL = 10000;
    private const float Z_VAL = 10000;
    #endregion

    /// <summary>
    /// To optimize search, create a linear array that can be easily traversed.
    /// </summary>
    private PoolInstance[] searchArray;
    private int lastSearchIndex = 0;

    /// <summary>
    /// Whether the pool has finished initializing;
    /// </summary>
    public bool isInitialized = false;

    #region UNITY MESSAGES
    private void Awake()
    {
        if(createPoolOnAwake)
        {
            Initialize();
        }
    }

    public void Initialize()
    {
        List<PoolInstance> poolList = new List<PoolInstance>();
        foreach(PrefabPoolConfiguration p in prefabs)
        {
            //allocate memory then insantiate the pool members
            p.instances = new PoolInstance[p.capacity];
            CreatePool(p, p.prefab, p.capacity);
            poolList.AddRange(p.instances);
        }
        searchArray = poolList.ToArray();
        isInitialized = true;
        //Uncomment to debug
        //DebugArray();
    }
    #endregion

    /// <summary>
    /// Creates a pool of the specified game object.
    /// </summary>
    /// <param name="conf"></param>
    /// <param name="prefab"></param>
    /// <param name="capacity"></param>
    public void CreatePool(PrefabPoolConfiguration conf, UnityEngine.Object prefab, int capacity)
    {
        Transform prefabTransform = GetObjectTransform(prefab);

        for(int i = 0; i < capacity; i++)
        {
            UnityEngine.Object _instance = UnityEngine.GameObject.Instantiate(prefab, new Vector3(X_VAL, Y_VAL, Z_VAL), prefabTransform.rotation) as UnityEngine.Object;
            conf.instances[i] = new PoolInstance() { instance = _instance, inUse = false };
            GetObjectTransform(_instance).gameObject.SetActive(false);
        }
    }

    public Transform GetObjectTransform(UnityEngine.Object _object)
    {
        if (_object == null)
        {
            Debug.LogError("You are trying to get the transform of a null object");
        }

        Transform prefabTransform;
        if(_object is Component)
        {
            prefabTransform = (_object as Component).transform;
        }
        else if(_object is Transform)
        {
            prefabTransform = (_object as Transform);
        }
        else if(_object is GameObject)
        {
            prefabTransform = (_object as GameObject).transform;
        }
        else
        {
            throw new System.Exception("Impossible to get this object transform. Type: " + _object.GetType());
        }
        return prefabTransform;
    }

    /// <summary>
    /// Takles an instantiated object from the array and returns it.
    /// </summary>
    /// <param name="prefab">The original prefab</param>
    /// <param name="position">Object's position</param>
    /// <param name="rotation">Object's rotation</param>
    /// <returns></returns>
    public UnityEngine.Object Instantiate(UnityEngine.Object prefab, Vector3 position, Quaternion rotation)
    {
        for(int i = 0; i < prefabs.Length; i++)
        {
            if(GetObjectTransform(prefabs[i].prefab) == GetObjectTransform(prefab))
            {
                //mark object as found
                PoolInstance p_inst;
                int j = 0;
                do
                {
                    p_inst = prefabs[i].instances[prefabs[i].it++ % prefabs[i].capacity];
                    j++;
                } while (p_inst.inUse && j < prefabs[i].capacity);
                //print(j);
                if(j < prefabs[i].capacity)
                {
                    p_inst.inUse = true;

                    Transform t = GetObjectTransform(p_inst.instance);
                    //print(p_inst.instance + "; " + t);
                    t.position = position;
                    t.rotation = rotation;
                    t.gameObject.SetActive(true);
                    return p_inst.instance;
                }
                else
                {
                    Debug.LogWarning("Pool has run out of the specified prefab " + prefab.GetInstanceID().ToString());
                    return null;
                }
            }
        }

        Debug.LogError("The object you are trying to instantiate is not in the pool " + prefab);
        return null;
    }

    public void Destroy(UnityEngine.Object _object)
    {
        //Uncomment to debug
        /*DebugArray();
        print("Will destroy " + _object);*/

        bool found = false;
        int i = 0;
        PoolInstance p_inst = null;
        while(!found && i++ < searchArray.Length)
        {
            p_inst = searchArray[lastSearchIndex++ % searchArray.Length];
            if (p_inst.instance != null && _object != null)
                found = GetObjectTransform(p_inst.instance) == GetObjectTransform(_object);
            else
                found = false;
        }
        if(i >= searchArray.Length && !found)
        {
            Debug.LogWarning("Could not reassign and relocate selected object @ pool destruction. Game may be unloading scene.");
        }
        if(found)
        {
            //Uncomment to debug
            //Debug.Log("Succesfully removed object at " + lastSearchIndex + ": " + p_inst);
            GetObjectTransform(p_inst.instance).position = new Vector3(X_VAL, Y_VAL, Z_VAL);
            p_inst.inUse = false;
            GetObjectTransform(p_inst.instance).gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Prints the array to console.
    /// </summary>
    void DebugArray()
    {
        string res = "";
        foreach(var o in searchArray)
        {
            res += o.instance + ":" + o.instance.GetInstanceID() + " | ";
        }
        print(res);
    }
}