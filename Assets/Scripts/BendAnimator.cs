﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A single class to create a unique bend animation that can be used by all the objects in the game.
/// </summary>
public class BendAnimator : MonoBehaviour {

    #region BENDING ANGLE
    private float m_Angle;
    /// <summary>
    /// The angle that will be used to animate all spawned objects that have an SpawnBend instance.
    /// </summary>
    public float Angle
    {
        get
        {
            return m_Angle;
        }
    }

    private const float MAX_DURATION = 4;
    private const float MIN_DURATION = 1.25f;
    private const float MIN_ANGLE = 20;
    private const float MAX_ANGLE = 40;

    private float direction = 1;
    #endregion

    void Start()
    {
        NewBendInvoke();
    }

    /// <summary>
    /// This method starts an animation bend loop.
    /// </summary>
    void NewBendInvoke()
    {
        direction *= -1;
        var to = Random.Range(MIN_ANGLE, MAX_ANGLE) * direction;
        var duration = Random.Range(MIN_DURATION, MAX_DURATION);
        StartCoroutine(BendTo(to, duration));
    }

    IEnumerator BendTo(float to, float duration)
    {
        var from = m_Angle;
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            m_Angle = Mathf.SmoothStep(from, to, t);
            yield return null;
        }
        NewBendInvoke();
    }
}