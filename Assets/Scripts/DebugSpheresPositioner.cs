﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OpenCVForUnity;

public class DebugSpheresPositioner : MonoBehaviour {

    private Vector3 initialPos;
    private TrackSilhouette tracker;

    public Transform prefab;
    public int maxCount = 200;
    public List<Transform> cubes = new List<Transform>();

    [Tooltip("Scale applied for positioning, not to scale at all.")]
    public Vector2 scale = new Vector2(2, 2);

    public static DebugSpheresPositioner instance
    {
        get;
        private set;
    }

    void Awake()
    {
        initialPos = prefab.transform.position;
        instance = this;
        tracker = FindObjectOfType<TrackSilhouette>();
    }

	void Start () {
        if (!enabled) return;

        for(int i = 0; i < maxCount; i++)
        {
            var p = Instantiate(prefab);
            p.gameObject.name = "Cube_" + i.ToString();
            cubes.AddRange(new Transform[] { p.transform });
            p.transform.parent = transform;
            p.transform.localPosition = Vector3.one * 10000;
        }
	}

	public void PositionCubes(Point[] positions)
    {
        if (!enabled) return;
        var len = Mathf.Clamp(positions.Length,0,maxCount);
        for(int i = 0; i < len; i++)
        {
            var pos = new Vector2((float)positions[i].x, (float)positions[i].y);
            var offset = new Vector2(-tracker.SensorWidth, -tracker.SensorHeight);
            pos = Vector2.Scale(pos, scale) + offset;
            cubes[i].position = new Vector3(pos.x, pos.y, initialPos.z);
        }
    }

}
