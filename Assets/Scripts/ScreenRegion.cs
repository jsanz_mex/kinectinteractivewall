﻿using System;
using UnityEngine;
using System.Collections;

using OpenCVForUnity;
using System.Collections.Generic;

public class ScreenRegion
{
    private const double MAX_SQR_DISTANCE = 0.5;

    private int x, y;
    private float timeOccupied = 0;
    private bool isOccupied = false;
    private Point lastCentroid = new Point{x=0, y=0};

    /// <summary>
    /// Unity instantiated prefab that occupies the region.
    /// </summary>
    public UnityEngine.Object currentObject;

    /// <summary>
    /// Current points 
    /// </summary>
    private Point[] points = new Point[0];

    public Point centroid
    {
        get
        {
            double xSum = 0.0, ySum = 0.0;
            for (int i = 0; i < points.Length; i++)
            {
                xSum += points[i].x;
                ySum += points[i].y;
            }
            return new Point(xSum / points.Length, ySum / points.Length);
        }
    }

    /// <summary>
    /// Direction of the local Right unit vector
    /// </summary>
    private Vector3[] directions = new Vector3[0];

    /// <summary>
    /// Resulting direction of this region
    /// </summary>
    public Vector3 direction
    {
        get
        {
            Vector3 sum = Vector3.zero;
            for (int i = 0; i < directions.Length; i++)
            {
                sum += directions[i];
            }
            if (sum.sqrMagnitude < 0.00f)
            {
                return Vector3.right;
            }

            return sum;
        }
    }

    private double getSqrDistance(Point a, Point b)
    {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        return dx * dx + dy * dy;
    }

    #region API

    /// <summary>
    /// Adds points to the 
    /// </summary>
    public void AddPoint(Point point, Vector3 direction)
    {
        var list = new List<Point>(points);
        list.Add(point);
        points = list.ToArray();

        var directionsList = new List<Vector3>(directions);
        directionsList.Add(direction);
        directions = directionsList.ToArray();
    }

    /// <summary>
    /// Update executed at the end of every frame.
    /// </summary>
    public void OnLateUpdate(float deltaTime)
    {
        if (!isOccupied)
        {
            //=======STATE: REGION HOVERED, IN PROCESS OF BEING OCCUPIED =======
            if (points.Length > 0 /*&& getSqrDistance(centroid, lastCentroid) < MAX_SQR_DISTANCE*/)
            {
                //Debug.Log("occupying, " + centroid + "; " + lastCentroid + "; " + getSqrDistance(centroid, lastCentroid).ToString());
                timeOccupied += deltaTime;
                if (timeOccupied > KinectWall.Constants.REGION_OCCUPIED_LOAD_TIME)
                {
                    timeOccupied = KinectWall.Constants.REGION_OCCUPIED_UNLOAD_TIME; // Old value: REGION_OCCUPIED_LOAD_TIME
                    isOccupied = true;
                    if (OnEnter != null)
                    {
                        OnEnter(this);
                    }
                }
            }
            //=======STATE: EMPTY IDLE =======
            /*else
            {
            }*/
        }
        else
        {
            //=======STATE: REGION UNHOVERED, IN PROCESS OF BEING DISOCCUPIED =======
            if (points.Length == 0)
            {
                timeOccupied -= deltaTime;
                if (timeOccupied < 0)
                {
                    timeOccupied = 0;
                    isOccupied = false;
                    if (OnExit != null)
                    {
                        //Reset region
                        OnExit(this);
                        currentObject = null;
                    }
                }
            }
            //=======STATE: BEING OCCUPIED =======
            /*else
            {
            }*/
        }

        //Cache last centroid
        //lastCentroid.x = centroid.x;
        //lastCentroid.y = centroid.y;

        //Always clear points and directions after every frame has finished
        points = new Point[0];
        directions = new Vector3[0];
    }

    /// <summary>
    /// Event triggered when the region has been occupied for more tahn REGION_OCCUPIED_LOAD_TIME time.
    /// </summary>
    public System.Action<ScreenRegion> OnEnter = null;

    /// <summary>
    /// Event triggered when the region has been left for more than REGION_OCCUPIED_LOAD_TIME time.
    /// </summary>
    public System.Action<ScreenRegion> OnExit = null;

    /// <summary>
    /// Assigns coordinates to region on Construction
    /// </summary>
    public ScreenRegion(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    #endregion
}