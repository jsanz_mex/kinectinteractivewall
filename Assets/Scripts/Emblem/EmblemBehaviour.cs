﻿using UnityEngine;
using System.Collections;

public class EmblemBehaviour : MonoBehaviour {

    private const float ALPHA_DURATION_IN = 1f;
    private const float ALPHA_DURATION_OUT = 0.75f;
    private const float SCALE_DURATION_IN = 0.75f;
    private const float SCALE_DURATION_OUT = 0.75f;

    private SpriteRenderer sprite;
    private Vector3 initialScale;

    public AnimationCurve alphaCurve = new AnimationCurve(new Keyframe(0,0), new Keyframe(1,1));
    public AnimationCurve scaleCurveIn = new AnimationCurve(new Keyframe(0,0), new Keyframe(1,1));
    public AnimationCurve scaleCurveOut = new AnimationCurve(new Keyframe(0,0), new Keyframe(1,1));

	void Start () {

        var tracker = FindObjectOfType<TrackSilhouette>();
        if(tracker)
        {
            //tracker.OnPlayerEnter += OnPlayerEnter;
            //tracker.OnPlayerExit += OnPlayerExit;
            KinectManager.Instance.OnPlayerEnter += OnPlayerEnter;
            KinectManager.Instance.OnPlayerExit += OnPlayerExit;
        }
        sprite = GetComponent<SpriteRenderer>();
        initialScale = transform.localScale;
    }

    void OnPlayerEnter()
    {
        print("Player enter.");
        StartCoroutine(AlphaTo(1, 0, ALPHA_DURATION_OUT, alphaCurve));
        StartCoroutine(ScaleTo(initialScale, Vector3.zero, SCALE_DURATION_OUT, scaleCurveOut));
    }

    void OnPlayerExit()
    {
        print("Player exit.");
        StartCoroutine(AlphaTo(0, 1, ALPHA_DURATION_IN, alphaCurve));
        StartCoroutine(ScaleTo(Vector3.zero, initialScale, SCALE_DURATION_IN, scaleCurveIn));
    }

    IEnumerator AlphaTo(float from, float to, float duration, AnimationCurve curve)
    {
        float t = 0;
        Color fromC = new Color(1, 1, 1, from);
        Color toC = new Color(1, 1, 1, to);
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            sprite.color = Color.Lerp(fromC, toC, curve.Evaluate(t));
            yield return null;
        }
    }

    IEnumerator ScaleTo(Vector3 from, Vector3 to, float duration, AnimationCurve curve)
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            transform.localScale = Vector3.Lerp(from, to, curve.Evaluate(t));
            yield return null;
        }
    }

#if UNITY_EDITOR
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Alpha1)) OnPlayerEnter();
    //    if (Input.GetKeyDown(KeyCode.Alpha2)) OnPlayerExit();
    //}
#endif
}
