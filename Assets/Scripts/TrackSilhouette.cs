﻿/******
 * HOW THIS WORKS:
 * The way to achieve this is:
 * 1. A grid where the contour is analized per cell. Every time a piece of contour enters the cell it starts adding time until it pops a flower.
 * 2. Scale animation curves that accelerate at the beggining of the animation when spawning in. Spawning out has a curve that accelerates at the end.
 */
using System;
using UnityEngine;
using System.Collections;

using OpenCVForUnity;
using System.Collections.Generic;

using Frankfort.Threading;
using System.Threading;

public class TrackSilhouette : MonoBehaviour {

    /// <summary>
    /// The web cam device.
    /// </summary>
    WebCamDevice webCamDevice;

    /// <summary>
    /// The colors.
    /// </summary>
    Color32[] colors;

    /// <summary>
    /// Should use front facing.
    /// </summary>
    public bool shouldUseFrontFacing = false;

    /// <summary>
    /// The width.
    /// </summary>
    int width = 640;

    /// <summary>
    /// The height.
    /// </summary>
    int height = 480;

    /// <summary>
    /// The rgba mat.
    /// </summary>
    Mat rgbMat;

    /// <summary>
    /// The init done.
    /// </summary>
    bool initDone = false;

    /// <summary>
    /// The screenOrientation.
    /// </summary>
    ScreenOrientation screenOrientation = ScreenOrientation.Unknown;

    /// <summary>
    /// max number of objects to be detected in frame
    /// </summary>
    const int MAX_NUM_OBJECTS = 50;

    /// <summary>
    /// minimum and maximum object area
    /// </summary>
    const int MIN_OBJECT_AREA = 20 * 20;

    /// <summary>
    /// max object area
    /// </summary>
    int MAX_OBJECT_AREA;

    /// <summary>
    /// The threshold mat.
    /// </summary>
    Mat thresholdMat;

    /// <summary>
    /// The hsv mat.
    /// </summary>
    Mat hsvMat;

    // buffer for the raw foreground image
    private byte[] foregroundImage;

    // the foreground texture
    private Texture2D foregroundTex;

    // rectangle taken by the foreground texture (in pixels)
    private UnityEngine.Rect foregroundRect;

    // primary sensor data structure
    private KinectInterop.SensorData sensorData = null;

    // Bool to keep track whether Kinect and BR library have been initialized
    private bool isBrInited = false;

    [Tooltip("Index of the player, tracked by this component. -1 means all players, 0 - the 1st player only, 1 - the 2nd player only, etc.")]
    public int playerIndex = -1;

    [Tooltip("Whether the hi-res (color camera resolution) is preferred for the foreground image. Otherwise the depth camera resolution will be used.")]
    public bool colorCameraResolution = true;

    [Tooltip("Camera used to display the foreground texture on the screen. Leave empty, if on-screen display of the foreground texture is not required.")]
    public Camera foregroundCamera;

    [Tooltip("Color used to paint pixels, where the foreground color data is not available.")]
    public Color32 defaultColor = new Color32(64, 64, 64, 255);

    [Tooltip("GUI-Text to display Image Processing fps")]
    public GUIText imageProcessingFPS_Text;

    [Tooltip("GUI-Text to display Game fps")]
    public GUIText gameFPS_Text;

    #region ANIMATION CONTROL
    float timeStill = 0;
    float timeShowing = 0;
    float MAX_STILL_TIME = 0.75f;
    bool isShowing = false;
    int MIN_CONTOUR_POINTS = 300;
    #endregion

    #region SENSOR SIZES
    public class TextureSize
    {
        public int width, height;
    }

    public static int[] availableWidths     = new int[] { 320, 640, 512, 1024, 1920 };
    public static int[] availableHeights    = new int[] { 240, 480, 424, 768, 1080 };
    public static TextureSize textureSize(int dataSize)
    {
        int i = 0;
        while( i < availableWidths.Length)
        {
            var size = availableWidths[i] * availableHeights[i];
            if (dataSize == size)
                return new TextureSize() { width=availableWidths[i], height=availableHeights[i] };
            i++;
        }
        return new TextureSize() { width = 640, height = 480 };
    }
    public int sensorWidth = 1920;
    public int sensorHeight = 1080;

    public int SensorWidth
    {
        get
        {
            return sensorWidth;
        }
    }

    public int SensorHeight
    {
        get
        {
            return sensorHeight;
        }
    }
    #endregion 

    #region ASYNC
    private Thread trackerThread;
    private float trackerUpdateTime, trackerStartTime;
    #endregion

    // Use this for initialization
    void Start()
    {
        //Set number of threads to OpenCV
        Core.setNumThreads(Core.getNumberOfCPUs() * 2);
        print("Makign the number of threads the double the CPUs: " + Core.getNumThreads() + " : " + Core.getNumberOfCPUs());

        initDone = false;

        try
        {
            // get sensor data
            KinectManager kinectManager = KinectManager.Instance;
            if (kinectManager && kinectManager.IsInitialized())
            {
                sensorData = kinectManager.GetSensorData();
            }

            if (sensorData == null || sensorData.sensorInterface == null)
            {
                throw new Exception("Background removal cannot be started, because KinectManager is missing or not initialized.");
            }

            // ensure the needed dlls are in place and speech recognition is available for this interface
            bool bNeedRestart = false;
            bool bSuccess = sensorData.sensorInterface.IsBackgroundRemovalAvailable(ref bNeedRestart);

            if (bSuccess)
            {
                if (bNeedRestart)
                {
                    KinectInterop.RestartLevel(gameObject, "BR");
                    return;
                }
            }
            else
            {
                string sInterfaceName = sensorData.sensorInterface.GetType().Name;
                throw new Exception(sInterfaceName + ": Background removal is not supported!");
            }

            // Initialize the background removal
            bSuccess = sensorData.sensorInterface.InitBackgroundRemoval(sensorData, colorCameraResolution);

            if (!bSuccess)
            {
                throw new Exception("Background removal could not be initialized.");
            }

            // create the foreground image and alpha-image
            int imageLength = sensorData.sensorInterface.GetForegroundFrameLength(sensorData, colorCameraResolution);
            foregroundImage = new byte[imageLength];

            //OPTIMIZATION
            //Make sure the Kinect controller is not computing or displaying any map, as this invokes Graphics.Blit(), which punishes Unity's main thread compute time.
            kinectManager.computeColorMap = false;
            kinectManager.computeInfraredMap = false;
            kinectManager.displayColorMap = false;
            kinectManager.displayUserMap = false;
            kinectManager.displaySkeletonLines = false;

            //KINECT CONFIGURATION
            //This configures the application to work correctly
            kinectManager.detectClosestUser = false;
            kinectManager.showTrackedUsersOnly = true;
            kinectManager.maxTrackedUsers = -1;     // 2 for kinect v1 || 6 for kinect v2


            //ORIGINAL METHOD
            // get the needed rectangle
            //UnityEngine.Rect neededFgRect = sensorData.sensorInterface.GetForegroundFrameRect(sensorData, colorCameraResolution);
            //print(neededFgRect.width + "; " + neededFgRect.height);
            //UnityEngine.Rect neededFgRect = sensorData.sensorInterface.GetForegroundFrameRect(sensorData, colorCameraResolution);
            //print(neededFgRect.width + "; " + neededFgRect.height);

            //NEW METHOD
            // normally it would be the color camera resolution thw one we use for texture size calculation(as aboves). But in our case it is the camera used for body index.
            // sensorData.bodyIndexImage

            // create the foreground texture ( TextureFormat.DXT5 works good when using 8 bits coming from user )
            var size = textureSize(sensorData.bodyIndexImage.Length);
            foregroundTex = new Texture2D(size.width, size.height, TextureFormat.RGBA32, false);
            foregroundTex.wrapMode = TextureWrapMode.Clamp;
            sensorWidth = size.width; sensorHeight = size.height;
            //foregroundTex.filterMode = FilterMode.Bilinear;

            // calculate the foreground rectangle
            if (foregroundCamera != null)
            {
                colors = new Color32[foregroundTex.width * foregroundTex.height];
                rgbMat = new Mat(foregroundTex.height, foregroundTex.width, CvType.CV_8UC4);
            }

            thresholdMat = new Mat();
            hsvMat = new Mat();

            MAX_OBJECT_AREA = (int)(foregroundTex.height * foregroundTex.width / 1.5);

            gameObject.GetComponent<Renderer>().material.mainTexture = foregroundTex;
            updateLayout();

            screenOrientation = Screen.orientation;

            isBrInited = true;
            initDone = true;
            print("Body index size: " + foregroundTex.width + "; " + foregroundTex.height);

            //Init screen regions
            InitScreenRegions();

            //Create Scheduler and start
            trackerThread = Loom.StartSingleThread(TrackSilhouetteCoroutine, System.Threading.ThreadPriority.Normal, true);

        }
        catch (DllNotFoundException ex)
        {
            Debug.LogError(ex.ToString());
            if (imageProcessingFPS_Text != null)
                imageProcessingFPS_Text.GetComponent<GUIText>().text = "Please check the Kinect and BR-Library installations.";
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
            if (imageProcessingFPS_Text != null)
                imageProcessingFPS_Text.GetComponent<GUIText>().text = ex.Message;
        }
    }



    private void updateLayout()
    {
        var ratio = (float)foregroundTex.width / (float)foregroundTex.height;
        gameObject.transform.localRotation = new Quaternion(0, 0, 0, 0);
        gameObject.transform.localScale = new Vector3(1080 * ratio, 1080, 0);
    }

    void OnDestroy()
    {
        if (isBrInited && sensorData != null && sensorData.sensorInterface != null)
        {
            // finish background removal
            sensorData.sensorInterface.FinishBackgroundRemoval(sensorData);
        }

        isBrInited = false;

        rgbMat.Dispose();
        thresholdMat.Dispose();
        hsvMat.Dispose();
    }

    bool once = false;

    
    void TrackSilhouetteCoroutine()
    {
        Debug.Log("Starting a new silhouette tracking Thread!");

        while(!initDone)
        { //waiting for initialization
        }
        while (true)
        {
            Loom.WaitForNextFrame();

            Loom.DispatchToMainThread(() =>
            {
                trackerUpdateTime = Time.realtimeSinceStartup - trackerStartTime;
                trackerStartTime = Time.realtimeSinceStartup;
            });

            //Uncomment ot debug
            //print(foregroundTex.width + "; " + foregroundTex.height + ";" + foregroundTex.GetRawTextureData().Length + "; " + sensorData.bodyIndexImage.Length);

            // BodyIndex texture is an 8 bit texture with monochromatic information about the user (in all Kinects).
            // However all the image processing is configured for a 32bit image where every color is detected independently.
            // Hence, to make image processing work We pass user data to the red portion and listen for red objects.
            // TO DO: Make calculations work with only 8 bits of monochromatic information.

            var len = sensorData.bodyIndexImage.Length;
            var data = new byte[len * 4];
            for (int i = 0; i < len; i++)
            {
                data[(i * 4) + 0] = (byte)(255 - (int)sensorData.bodyIndexImage[i]);
                data[(i * 4) + 1] = 0;
                data[(i * 4) + 2] = 0;
                data[(i * 4) + 3] = 255;
            }
            Loom.DispatchToMainThread(() =>
            {
                foregroundTex.LoadRawTextureData(data);
                foregroundTex.Apply();
                Utils.textureToMat(foregroundTex, rgbMat);
            }, true);

            //IMAGE PROCESS
            //No need of flipping
            //Core.flip(rgbMat, rgbMat, 1);
            ColorObject red = new ColorObject("red");
            Imgproc.cvtColor(rgbMat, hsvMat, Imgproc.COLOR_RGB2HSV);
            Core.inRange(hsvMat, red.getHSVmin(), red.getHSVmax(), thresholdMat);
            morphOps(thresholdMat);
            trackFilteredObject(red, thresholdMat, hsvMat, rgbMat);

            Loom.DispatchToMainThread(() =>
            {
                //Update texture
                Utils.matToTexture2D(rgbMat, foregroundTex, colors);
                //Update screen regions
                UpdateScreenRegions();
            }, true);
        }
    }

    void LateUpdate()
    {
        if (gameFPS_Text && Time.frameCount % 10 == 0)
        {
            gameFPS_Text.text = "FPS(g): " + Mathf.FloorToInt(1f / Time.smoothDeltaTime).ToString();
        }
    }

    /// <summary>
    /// Draws the object.
    /// </summary>
    /// <param name="theColorObjects">The color objects.</param>
    /// <param name="frame">Frame.</param>
    /// <param name="temp">Temp.</param>
    /// <param name="contours">Contours.</param>
    /// <param name="hierarchy">Hierarchy.</param>
    void drawObject(List<ColorObject> theColorObjects, Mat frame, Mat temp, List<MatOfPoint> contours, Mat hierarchy)
    {

        for (int i = 0; i < theColorObjects.Count; i++)
        {
            Imgproc.drawContours(frame, contours, i, theColorObjects[i].getColor(), 3, 8, hierarchy, int.MaxValue, new Point());
            Core.circle(frame, new Point(theColorObjects[i].getXPos(), theColorObjects[i].getYPos()), 5, theColorObjects[i].getColor());
            Core.putText(frame, theColorObjects[i].getXPos() + " , " + theColorObjects[i].getYPos(), new Point(theColorObjects[i].getXPos(), theColorObjects[i].getYPos() + 20), 1, 1, theColorObjects[i].getColor(), 2);
            Core.putText(frame, theColorObjects[i].getType(), new Point(theColorObjects[i].getXPos(), theColorObjects[i].getYPos() - 20), 1, 2, theColorObjects[i].getColor(), 2);
        }
    }

    /// <summary>
    /// Morphs the ops.
    /// </summary>
    /// <param name="thresh">Thresh.</param>
    void morphOps(Mat thresh)
    {

        //create structuring element that will be used to "dilate" and "erode" image.
        //the element chosen here is a 3px by 3px rectangle
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        //dilate with larger element so make sure object is nicely visible
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8));

        Imgproc.erode(thresh, thresh, erodeElement);
        Imgproc.erode(thresh, thresh, erodeElement);

        Imgproc.dilate(thresh, thresh, dilateElement);
        Imgproc.dilate(thresh, thresh, dilateElement);
    }

    private List<double> perimetersArray = new List<double>();

    /// <summary>
    /// Tracks the filtered object.
    /// </summary>
    /// <param name="theColorObject">The color object.</param>
    /// <param name="threshold">Threshold.</param>
    /// <param name="HSV">HS.</param>
    /// <param name="cameraFeed">Camera feed.</param>
    void trackFilteredObject(ColorObject theColorObject, Mat threshold, Mat HSV, Mat cameraFeed)
    {
        List<ColorObject> colorObjects = new List<ColorObject>();
        Mat temp = new Mat();
        threshold.copyTo(temp);
        //these two vectors needed for output of findContours
        List<MatOfPoint> contours = new List<MatOfPoint>();
        Mat hierarchy = new Mat();
        //find contours of filtered image using openCV findContours function
        Imgproc.findContours(temp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
        //use moments method to find our filtered object
        bool colorObjectFound = false;
        if (hierarchy.rows() > 0)
        {
            int numObjects = hierarchy.rows();

            //						Debug.Log("hierarchy " + hierarchy.ToString());

            //if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
            if (numObjects < MAX_NUM_OBJECTS)
            {
                for (int index = 0; index >= 0; index = (int)hierarchy.get(0, index)[0])
                {

                    Moments moment = Imgproc.moments(contours[index]);
                    double area = moment.get_m00();

                    //if the area is less than 20 px by 20px then it is probably just noise
                    //if the area is the same as the 3/2 of the image size, probably just a bad filter
                    //we only want the object with the largest area so we safe a reference area each
                    //iteration and compare it to the area in the next iteration.
                    if (area > MIN_OBJECT_AREA)
                    {

                        ColorObject colorObject = new ColorObject();

                        colorObject.setXPos((int)(moment.get_m10() / area));
                        colorObject.setYPos((int)(moment.get_m01() / area));
                        colorObject.setType(theColorObject.getType());
                        colorObject.setColor(theColorObject.getColor());

                        colorObjects.Add(colorObject);

                        colorObjectFound = true;

                    }
                    else
                    {
                        colorObjectFound = false;
                    }
                }
                //let user know you found an object
                if (colorObjectFound == true)
                {
                    //draw object location on screen
                    drawObject(colorObjects, cameraFeed, temp, contours, hierarchy);

                    //Uncomment to debug
                    /*if (contours.Count > 0)
                    {
                        var points = contours[0].toArray();
                        /*string res = "";
                        foreach (var p in points)
                        {
                            res += string.Format("[x:{0}, y:{1}], ", p.x, p.y);
                        }
                        print(points.Length + ": " + res);
                        DebugSpheresPositioner.instance.PositionCubes(points);
                    }*/

                    // NEW CONTOUR LOGIC:
                    // Send information about contour points to ScreenRegions
                    if (contours.Count > 0)
                    {
                        var lastPos = Vector3.zero;
                        for(int i = 0; i < contours.Count; i++)
                        {
                            var points = contours[i].toArray();
                            var len = points.Length;
                            for(int j = 0; j < len; j++)
                            {
                                var p = points[j];
                                var sr_x = (int) System.Math.Floor(p.x / screenRegionsSize.x);
                                var sr_y = (int) System.Math.Floor(p.y / screenRegionsSize.y);
                                var pos = new Vector3((float)p.x, (float)p.y, 0);
                                if (j > 0)
                                {
                                    screenRegions[sr_x, sr_y].AddPoint(p, pos - lastPos);
                                }
                                lastPos = pos;
                            }
                        }
                    }
                }
            }
            else
            {
                Core.putText(cameraFeed, "TOO MUCH NOISE!", new Point(5, cameraFeed.rows() - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar(255, 255, 255, 255), 2, Core.LINE_AA, false);
            }
        }
    }

    void UpdateScreenRegions()
    {
        for(int i = 0; i < screenRegions.GetLength(0); i ++)
        {
            for(int j = 0; j < screenRegions.GetLength(1); j ++)
            {
                if(imageProcessingFPS_Text && Time.frameCount % 10 == 0)
                {
                    imageProcessingFPS_Text.text = "FPS(i): " + Mathf.FloorToInt(1f / trackerUpdateTime).ToString();
                }
                screenRegions[i, j].OnLateUpdate(trackerUpdateTime);
            }
        }
    }

    #region Screen Region Arrays

    public class ScreenRegionsSize
    {
        public double x;
        public double y;
    }

    ScreenRegionsSize screenRegionsSize = new ScreenRegionsSize();

    /// <summary>
    /// A multidimensional array of pre-generated regions.
    /// </summary>
    ScreenRegion[,] screenRegions;

    void InitScreenRegions()
    {
        int horizontalRegions = Mathf.FloorToInt( (float)SensorWidth * (float)KinectWall.Constants.NUMBER_OF_VERTICAL_REGIONS / (float)SensorHeight );
        screenRegions = new ScreenRegion[horizontalRegions, KinectWall.Constants.NUMBER_OF_VERTICAL_REGIONS];
        for (int i = 0; i < horizontalRegions; i++)
        {
            for (int j = 0; j < KinectWall.Constants.NUMBER_OF_VERTICAL_REGIONS; j++)
            {
                screenRegions[i, j] = new ScreenRegion(i,j);
                screenRegions[i, j].OnEnter += SilhouetteSpawner.instance.CreateObjectAtRegion;
                screenRegions[i, j].OnExit += SilhouetteSpawner.instance.DestroyObjectAtRegion;
            }
        }

        screenRegionsSize.x = (double)SensorWidth / (double)horizontalRegions;
        screenRegionsSize.y = (double)SensorHeight / (double)KinectWall.Constants.NUMBER_OF_VERTICAL_REGIONS;
        Debug.Log("Screen regions: " + screenRegionsSize.x + ", " + screenRegionsSize.y);
    }
    #endregion
}

public class TrackingWorker : IThreadWorkerObject
{
    public int MaxThreads = -1;
    public bool safeMode = true;

    public void ExecuteThreadedWork()
    {
        throw new NotImplementedException();
    }

    public void AbortThreadedWork()
    {
        throw new NotImplementedException();
    }
}