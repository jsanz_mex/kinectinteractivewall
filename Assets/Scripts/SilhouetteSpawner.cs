﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;

/// <summary>
/// Object spawned at the edge of the silhoutte
/// </summary>
[System.Serializable]
public class EdgeObject
{
    public Transform prefab;
    public float width = 50;
}

public class SilhouetteSpawner : MonoBehaviour {

    [Tooltip("Scale applied for positioning, not to scale at all.")]
    public Vector2 scale = new Vector2(2, 2);
    private GameObjectsPool pool;

    private TrackSilhouette tracker;

    public static SilhouetteSpawner instance
    {
        get;
        private set;
    }

    void Awake()
    {
        instance = this;
        tracker = FindObjectOfType<TrackSilhouette>();
        pool = GetComponent<GameObjectsPool>();
    }

    //Uncomment and correct in case you need this method
    /*public void CreateObjects(Point[] points)
    {
        print("Created objects for " + points.Length + " points.");
        var accDistance = 0f;
        var accDirection = Vector3.zero;
        var index = Random.Range(0, edgeObjects.Length);
        var halfDistance = edgeObjects[index].width / 2f;
        var fullDistance = edgeObjects[index].width;
        var lastPos = transform.position;
        Transform lastSpawn = null;
        var noMidPoint = true;
        for(int i = 0; i < points.Length; i++)
        {
            var point = new Vector2((float)points[i].x, (float)points[i].y);
            var offset = new Vector2(-tracker.SensorWidth, -tracker.SensorHeight);
            var pos2D = Vector2.Scale (point, scale) + offset;
            var pos3D = new Vector3(pos2D.x, pos2D.y, transform.position.z);
            if(i > 0)
            {
                var direction = pos3D - lastPos;
                accDistance += direction.magnitude;
                accDirection += direction;

                if(accDistance > halfDistance && noMidPoint)
                {
                    noMidPoint = false;
                    lastSpawn = Instantiate(edgeObjects[index].prefab) as Transform;
                    lastSpawn.position = pos3D;
                    lastSpawn.transform.parent = transform;
                    lastSpawn.BroadcastMessage("OnSpawn");
                }
                if(accDistance > fullDistance && !noMidPoint)
                {
                    noMidPoint = true;
                    lastSpawn.right = accDirection.normalized;
                    accDirection = Vector3.zero;
                    accDistance = 0;
                    index = Random.Range(0, edgeObjects.Length);
                }
            }
            lastPos = pos3D;
        }
    }*/

    public void CreateObjectAtRegion(ScreenRegion screenRegion)
    {
        //0. If assigned screen region has an object do not spawn anything
        if(screenRegion != null)
        {
            if(screenRegion.currentObject != null)
            {
                Debug.LogWarning("This screenRegion is already occupied. Cannot create another object.");
                return;
            }
        }
        else
        {
            Debug.LogWarning("The screen region you want to use is null");
            return;
        }

        //print("Created objects at: " + m_point.x + ", " + m_point.y);

        //1. Get prefab index
        var index = Random.Range(0, pool.prefabs.Length);

        //2. Calculate spawn position 
        //var point = new Vector2((float)m_point.x, (float)m_point.y);
        var point = new Vector2((float)screenRegion.centroid.x, (float)screenRegion.centroid.y);
        var offset = new Vector2(-tracker.SensorWidth, -tracker.SensorHeight);
        var pos2D = Vector2.Scale(point, scale) + offset;
        var pos3D = new Vector3(pos2D.x, pos2D.y, transform.position.z);
        
        //3. Spawn and notify spawned (only if succesfully spawned by the pool)
        //var lastSpawn = Instantiate(edgeObjects[index].prefab);
        var lastSpawn = pool.Instantiate(pool.prefabs[index].prefab, transform.position, transform.rotation);

        if (lastSpawn)
        {
            var lastSpawnTrans = pool.GetObjectTransform(lastSpawn);
            lastSpawnTrans.position = pos3D;
            lastSpawnTrans.right = screenRegion.direction;
            lastSpawnTrans.parent = transform;
            lastSpawnTrans.gameObject.BroadcastMessage("OnSpawn");

            //4. Subscribe object to its screen region
            //Uncomment to debug
            //print("Assigned " + lastSpawn + " : " + lastSpawn.GetInstanceID());
            screenRegion.currentObject = lastSpawn;
        }
        else
        {
            Debug.Log("The object you tried to spawn is not avaiulable in the pool. Consider increasing your pool size.");
        }
    }

    public void DestroyObjectAtRegion(ScreenRegion screenRegion)
    {
        //Old way does not consider the object comes from a pool
        /*screenRegion.currentObject.BroadcastMessage("SpawnOut");
        screenRegion.currentObject = null;*/

        //Uncomment to debug
        //print("Will Scale down " + screenRegion.currentObject + " : " + screenRegion.currentObject.GetInstanceID());
        
        var spawnScale = pool.GetObjectTransform(screenRegion.currentObject).GetComponent<SpawnScale>();

        //Uncomment to debug
        //print("Spawn scale: " + spawnScale + "; " + spawnScale.gameObject.GetInstanceID());
        
        //Unfortunately Unity is not processing anonymous methods as expected. In this case it is loosing reference to screenRegion.currentObject.
        //Switched to a non-anonymous method and early dettaching of screen region's object.
        spawnScale.SpawnOutMessage(ConcreteDestroyObject);
        screenRegion.currentObject = null;
    }

    void ConcreteDestroyObject(UnityEngine.Object _object)
    {
        //Uncomment to debug
        //print("On call back to destroy: " + _object);
        pool.Destroy(_object);
    }
}
