﻿using UnityEngine;
using System.Collections;

namespace KinectWall
{
    public class Constants
    {
        public const float SHOW_UP_DURATION = 30;
        public const float DESTRUCTION_DELAY = 0.25f;                   // Old value: 1
        public const float REGION_OCCUPIED_LOAD_TIME = 0.3f;            // Old value: 0.65f
        public const float REGION_OCCUPIED_UNLOAD_TIME = 0.15f;         // It is the time a single image analysis takes
        public const int NUMBER_OF_VERTICAL_REGIONS = 10;               // Old value: 20
    }
}