﻿using UnityEngine;
using System.Collections;

public class SpawnAlpha : MonoBehaviour {

    const string COLOR_PROPERTY = "_Color";
    const float MIN_DELAY = 0.5f;
    const float MAX_DELAY = 0.75f;
    const float DURATION_IN = 0.92f;    // Old value: 0.5f
    const float DURATION_OUT = 0.5f;   // Old value: 0.5f

    static Color CLEAR = new Color(1, 1, 1, 0);
    static Color WHITE = new Color(1, 1, 1, 1);

    public AnimationCurve curveIn = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public AnimationCurve curveOut = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public bool animateIn, animateOut;
    public bool enableInAnimation = true, enableOutAnimation = true;

    private Material material;

    private float randomDelay
    {
        get
        {
            return Random.Range(MIN_DELAY, MAX_DELAY);
        }
    }

    void Awake()
    {
        material = GetComponent<MeshRenderer>().material;
    }

    IEnumerator AlphaTo(float delay, Color from, Color to, float duration, AnimationCurve curve, float callbackDelay = 0, System.Action<UnityEngine.Object> callback = null)
    {
        yield return new WaitForSeconds(delay);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            Color newColor = Color.Lerp(from, to, curve.Evaluate(t));
            material.SetColor(COLOR_PROPERTY, newColor);
            yield return null;
        }
        yield return new WaitForSeconds(callbackDelay);
        if (callback != null)
            callback(gameObject as UnityEngine.Object);
    }

    void OnSpawn()
    {
        //Early out if IN animation is not enabled.
        if (!enableInAnimation) return;

        //Used to be delay but that makes animation pretty slow
        //StartCoroutine(ScaleTo(randomDelay, 0, 1, DURATION_IN));
        this.StopAllCoroutines();
        StartCoroutine(AlphaTo(0, CLEAR, WHITE, DURATION_IN, curveIn));
    }

    void SpawnOut()
    {
        //Early out if OUT animation is not enabled.
        if (!enableOutAnimation) return;

        this.StopAllCoroutines();
        StartCoroutine(AlphaTo(0, WHITE, CLEAR, DURATION_OUT, curveOut, KinectWall.Constants.DESTRUCTION_DELAY, SelfDestroy));
    }

    public void SpawnOutMessage(System.Action<UnityEngine.Object> callback)
    {
        //Early out if OUT animation is not enabled.
        if (!enableOutAnimation) return;

        this.StopAllCoroutines();
        StartCoroutine(AlphaTo(0, WHITE, CLEAR, DURATION_OUT, curveOut, KinectWall.Constants.DESTRUCTION_DELAY, callback));
    }

    void SelfDestroy(UnityEngine.Object _object)
    {
        Destroy(gameObject);
    }

#if UNITY_EDITOR
    void Update()
    {
        if (animateIn)
        {
            animateIn = false;
            OnSpawn();
        }

        if (animateOut)
        {
            animateOut = false;
            SpawnOut();
        }
    }
#endif
}
