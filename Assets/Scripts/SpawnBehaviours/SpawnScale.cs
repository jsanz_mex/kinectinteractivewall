﻿using UnityEngine;
using System.Collections;

public class SpawnScale : MonoBehaviour {

    const float MIN_DELAY = 0.5f;
    const float MAX_DELAY = 0.75f;
    const float DURATION_IN = 0.92f;    // Old value: 0.5f
    const float DURATION_OUT = 0.5f;   // Old value: 0.5f

    public AnimationCurve curveIn = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public AnimationCurve curveOut = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public bool animateIn, animateOut;
    public bool enableInAnimation = true, enableOutAnimation = true;

    private Vector3 initialScale;

    private float randomDelay
    {
        get
        {
            return Random.Range(MIN_DELAY, MAX_DELAY);
        }
    }

    void Awake()
    {
        initialScale = transform.localScale;
        transform.localScale = Vector3.zero;   
    }

    IEnumerator ScaleTo(float delay, float from, float to, float duration, AnimationCurve curve, float callbackDelay = 0, System.Action<UnityEngine.Object> callback = null)
    {
        yield return new WaitForSeconds(delay);
        var fromV = initialScale * from;
        var toV = initialScale * to;
        float t = 0;
        while(t < 1)
        {
            t += Time.deltaTime / duration;
            transform.localScale = Vector3.Lerp(fromV, toV, curve.Evaluate(t));
            yield return null;
        }
        yield return new WaitForSeconds(callbackDelay);
        if (callback != null)
            callback(gameObject as UnityEngine.Object);
    }

    void OnSpawn()
    {
        //Early out if IN animation is not enabled.
        if (!enableInAnimation) return;

        //Used to be delay but that makes animation pretty slow
        //StartCoroutine(ScaleTo(randomDelay, 0, 1, DURATION_IN));
        this.StopAllCoroutines();
        StartCoroutine(ScaleTo(0, 0, 1, DURATION_IN, curveIn));
    }

    void SpawnOut()
    {
        //Early out if OUT animation is not enabled.
        if (!enableOutAnimation) return;

        this.StopAllCoroutines();
        StartCoroutine(ScaleTo(0, 1, 0, DURATION_OUT, curveOut, KinectWall.Constants.DESTRUCTION_DELAY, SelfDestroy));
    }

    public void SpawnOutMessage(System.Action<UnityEngine.Object> callback)
    {
        //Early out if OUT animation is not enabled.
        if (!enableOutAnimation) return;

        this.StopAllCoroutines();
        StartCoroutine(ScaleTo(0, 1, 0, DURATION_OUT, curveOut, KinectWall.Constants.DESTRUCTION_DELAY, callback));
    }

    void SelfDestroy(UnityEngine.Object _object)
    {
        Destroy(gameObject);
    }
    
#if UNITY_EDITOR
    void Update()
    {
        if (animateIn)
        {
            animateIn = false;
            OnSpawn();
        }

        if (animateOut)
        {
            animateOut = false;
            SpawnOut();
        }
    }
#endif
}
