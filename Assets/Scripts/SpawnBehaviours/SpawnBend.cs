﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MegaBend))]
public class SpawnBend : MonoBehaviour {

    MegaBend bend;

    [Range(0.01f,1f)]
    public float bendGain = 1;

    BendAnimator bendAnimator;

	// Use this for initialization
	void Start () {
        bend = GetComponent<MegaBend>();
        bendAnimator = gameObject.AddComponent<BendAnimator>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(bend && bendAnimator)
        {
            bend.angle = bendAnimator.Angle * bendGain;
        }
	}
}
