﻿using UnityEngine;
using System.Collections;

public class BottomFoliage : MonoBehaviour {

    public Transform[] prefabs;
    public Vector2 screenSize = new Vector2(1920,1080);

    private TrackSilhouette tracker;

    public bool animateIn, animateOut;

	// Use this for initialization
	void Start ()
    {
        this.tracker = FindObjectOfType<TrackSilhouette>();
        //Subcribe to tracker events
        if (this.tracker)
        {
            KinectManager.Instance.OnPlayerEnter += this.OnPlayerEnter;
            KinectManager.Instance.OnPlayerExit += this.OnPlayerExit;
            //this.tracker.OnPlayerEnter += this.OnPlayerEnter;
            //this.tracker.OnPlayerExit += this.OnPlayerExit;
        }
	}
	
    /// <summary>
    /// Make foliage because SpawnOut method destroy it
    /// </summary>
	void OnPlayerEnter () 
    {
        this.makeFoliage();
        gameObject.BroadcastMessage("OnSpawn");
	}

    void OnPlayerExit()
    {
        gameObject.BroadcastMessage("SpawnOut");
    }

    /// <summary>
    /// Make bottom foliage
    /// </summary>
    void makeFoliage()
    {
        var foliageCount = this.prefabs.Length * 2;
        var heightScreen = this.screenSize.x;
        var posY = -this.screenSize.y / 2;

        for (var i = 0; i < foliageCount; i++)
        {
            var index = (int)(Random.Range(0, this.prefabs.Length - 1));
            var posX = Random.Range(-heightScreen / 2, heightScreen / 2);
            var position = new Vector3(posX, posY, 0);

            var plant = Instantiate(this.prefabs[index], position, this.prefabs[index].transform.rotation) as Transform;
            plant.transform.parent = this.transform;
        }
    }

    #if UNITY_EDITOR
    void Update()
    {
        if (this.animateIn)
        {
            this.OnPlayerEnter();
            this.animateIn = false;
        }

        if (this.animateOut)
        {
            this.OnPlayerExit();
            this.animateOut = false;
        }
    }
    #endif
}
